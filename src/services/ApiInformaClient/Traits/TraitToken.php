<?php

namespace Borrox\GetPredictiveCompaniesInforma\Services\ApiInformaClient\Traits;

use GuzzleHttp\Client;
use Illuminate\Support\Facades\Cache;
use Borrox\GetPredictiveCompaniesInforma\Services\ApiInformaClient\Exceptions\InvalidClientRequestApiInforma;

trait TraitToken
{

    public function saveToken($token)
    {
        $expiresAt = now()->addMinutes(60);

        Cache::put('APIINFORMA:TOKEN',$token, $expiresAt);

        return $token;
    }

    public function readToken()
    {
        return Cache::get('APIINFORMA:TOKEN');
    }

    public function removeToken()
    {
        return Cache::forget('APIINFORMA:TOKEN');
    }


    public function getToken()
    {
        if(!$this->readToken()){
            return $this->generateToken();
        }

        return $this->readToken();
    }

    public function generateToken()
    {
        $guzzle = new Client;


        $params = [
            'grant_type'    => 'client_credentials',
            'client_id'     => $this->getClientId(),
            'client_secret' => $this->getClientSecret(),
            'scope' => 'manage-assignors manage-debtors manage-cessions manage-concilliations manage-invoices manage-financial-requests manage-promisory-notes manage-debts sepa',
        ];

        $this->response = $guzzle->post($this->getUrlAccessToken(), [
            'form_params' => $params,
            'http_errors' => $this->getHttpErrors(),
        ]);




        activity()
        ->causedBy(auth()->user())
        ->withProperties([
            'uri' => $this->getUrlAccessToken(),
            'params' => $params,
            'method' => __FUNCTION__,
            'status_code' => $this->response->getStatusCode(),
            'response' => $this->responsePretty()
        ])
        ->log('Llamada al api Summa | Generar token');


        if ($this->response->getStatusCode() == 200) {
            $token = json_decode((string) $this->response->getBody(), true)['access_token'];
            $this->saveToken($token);
        } else {
             throw new InvalidClientRequestApiInforma("Error Processing Request API INFORMA", 0, null, $this->response);
        }

        return $token;
    }
}
