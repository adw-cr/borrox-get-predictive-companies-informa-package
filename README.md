# GET PREDICTIVES PACKAGE
  
Paquete (SDK) para consumir el api de informa y algunos de sus productos.

  - Basado en modelos
  - Para php puro o Laravel

### Denpendicias

* "php" : ">=5.6.4",
* "guzzlehttp/guzzle" : "^6.3",
 * "spatie/laravel-activitylog": "^3.2" (OPTIONAL)

### Installation

Via Composer

``` bash
$ composer require borrox/get-predictive-companies-informa
```
