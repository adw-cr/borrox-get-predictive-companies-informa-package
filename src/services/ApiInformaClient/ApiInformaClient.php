<?php

namespace Borrox\GetPredictiveCompaniesInforma\Services\ApiInformaClient;

use GuzzleHttp\Client;
use Borrox\GetPredictiveCompaniesInforma\Services\ApiInformaClient\Traits\TraitToken;
use Borrox\GetPredictiveCompaniesInforma\Services\ApiInformaClient\Traits\HelpersClient;
use Borrox\GetPredictiveCompaniesInforma\Services\ApiInformaClient\Traits\VerbsHttpClient;
use Borrox\GetPredictiveCompaniesInforma\Services\ApiInformaClient\Exceptions\InvalidClientRequestApiInforma;

class ApiInformaClient
{
    use TraitToken, HelpersClient, VerbsHttpClient;

    protected $client;
    public $response;

    protected $disabledLog;

    public function __construct()
    {
    }

    public function initClient()
    {
        $this->client = new Client([
            'base_uri'    => $this->getApiBaseUri(),
            'headers'     =>
            [
                //'Authorization' => "Bearer " . $this->getToken(), //. env('APIINFORMA_API_TOKEN'),
                'content-type'  => 'application/json',
                'Content-Language' => 'es'
            ],
            'http_errors' => $this->getHttpErrors(),
        ]);
    }


    public function getApiBaseUri()
    {
        return env('APIINFORMA_API_BASE_URI');
    }

    public function getHttpErrors()
    {
        return false;
    }

    public function getUsername()
    {
        return env('APIINFORMA_USERNAME');
    }
    public function getPassword()
    {
        return env('APIINFORMA_PASSWORD');
    }

    public function getUrlAccessToken()
    {
        return $this->getApiBaseUri() . 'login';
    }


    public function disabledLog()
    {
        $this->disabledLog = true;
        return $this;
    }
    public function middlewareTerminateRequest($uri, $params, $method)
    {
        if(!$this->disabledLog){
            activity()
            ->causedBy(auth()->user())
            ->withProperties([
                'uri' => $uri,
                'params' => $params,
                'method' => $method,
                'status_code' => $this->response->getStatusCode(),
                'response' => $this->responsePretty()
            ])
            ->log('Llamada al api Informa | Uri: '.$uri);
        }


        if ($this->response->getStatusCode() == 401) {

            //$this->removeToken();
            //$this->initClient();

            //$this->$method($uri, $params);

        }
        if ($this->response->getStatusCode() >= 400) {
            throw new InvalidClientRequestApiInforma("Error Processing Request API INFORMA", 1, null, $this->response);
        }

    }
}
