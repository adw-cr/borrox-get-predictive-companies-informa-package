<?php

namespace Borrox\GetPredictiveCompaniesInforma\Services\ApiInformaClient\Traits;

trait VerbsHttpClient
{

    public function post($uri, $params = [])
    {
        $this->initClient();

        $this->response = $this->client->request('POST', $uri, [
            'json' => $params,
        ]);

        $this->middlewareTerminateRequest($uri, $params, __FUNCTION__);

        return $this;
    }

    public function get($uri, $params = [])
    {
        $this->initClient();

        $this->response = $this->client->request('GET', $uri, [
            'query' => $params,
        ]);
        $this->middlewareTerminateRequest($uri, $params, __FUNCTION__);

        return $this;
    }

    public function put($uri, $params = [])
    {

        $this->initClient();

        $this->response = $this->client->request('PUT', $uri, [
            'json' => $params,
        ]);

        $this->middlewareTerminateRequest($uri, $params, __FUNCTION__);

        return $this;

    }

    public function patch($uri, $params = [])
    {
        $this->initClient();

        $this->response = $this->client->request('PATCH', $uri, [
            'json' => $params,
        ]);

        $this->middlewareTerminateRequest($uri, $params, __FUNCTION__);

        return $this->response;
    }

    public function delete($uri,$data = [])
    {
        $this->initClient();

        $this->response = $this->client->request('DELETE', $uri,[
            'json' => $data,
        ]);

        $this->middlewareTerminateRequest($uri,$data, __FUNCTION__);

        return $this->response;
    }
}
