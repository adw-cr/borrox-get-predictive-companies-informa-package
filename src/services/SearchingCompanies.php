<?php

namespace Borrox\GetPredictiveCompaniesInforma\Services;

use Borrox\GetPredictiveCompaniesInforma\Services\ApiInformaClient\ApiInformaClient;

class SearchingCompanies
{
    protected $client;
    protected $response;

    public function __construct(ApiInformaClient $client) {
        $this->client = $client;
    }

    public function disabledLog()
    {
        $this->client->disabledLog();
        return $this;
    }

    public function get($term)
    {

        $this->response = $this->client->get('get-predictive-companies',[
            'username' => $this->client->getUsername(),
            'password' => $this->client->getPassword(),
            'denominacion' => $term,
        ]);


        $companies =  $this->response->responsePretty('datosProducto');

        return  $companies ? $companies  : [] ;
    }

    public function find($cif_encritp)
    {
        $this->response = $this->client->get('get-product?',[
            'username' => $this->client->getUsername(),
            'password' => $this->client->getPassword(),
            'product' => 'FICHA',
            'cif' => $cif_encritp,
        ]);

        return $this->response->responsePretty('datosProducto') ;
    }
}
