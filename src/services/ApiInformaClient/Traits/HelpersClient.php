<?php

namespace Borrox\GetPredictiveCompaniesInforma\Services\ApiInformaClient\Traits;

trait HelpersClient
{
    public function responsePretty($key = null)
    {
        $data = json_decode((string) $this->response->getBody(), true);

        if($key){
            return $data[$key];
        }

        return $data;
    }
    public function isCreated()
    {
        return ($this->response->getStatusCode() == 201) ? true : false;
    }

    public function getData()
    {
        return $this->responsePretty()['data'];
    }
}
