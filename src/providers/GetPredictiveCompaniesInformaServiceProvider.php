<?php

namespace Borrox\GetPredictiveCompaniesInforma\Providers;

use Illuminate\Support\ServiceProvider;

class GetPredictiveCompaniesInformaServiceProvider extends ServiceProvider
{
    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {

    }

    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }
}
